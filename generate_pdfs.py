#!/usr/bin/env python
import webbrowser
from multiprocessing import Process
from pathlib import Path

from server import start_server
from bills import bills


def generate_pdfs():
    Path("pdfs/").mkdir(exist_ok=True)

    server = Process(target=start_server)
    server.start()

    for bill in bills:
        id = bill.recipient.id
        name = bill.recipient.name.split(' ')[0]
        cmd = f'chromium --headless --no-sandbox --disable-gpu --print-to-pdf="pdfs/bill_{name}.pdf" %s'
        print(cmd)
        webbrowser.get(cmd).open_new(f'http://localhost:5000/bill/{id}')

    server.terminate()
    server.join()

if __name__ == '__main__':
    exit(generate_pdfs())
