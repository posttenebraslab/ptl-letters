from typing import List, Optional
from dataclasses import dataclass, field
from enum import Enum


@dataclass
class Recipient:
    id: int
    name: str
    street: str
    city: str
    pcode: str
    country: str = 'Suisse'

@dataclass
class Prestation:
    name: str
    quantity: float
    price: float
    unit: Optional[str] = ''
    description: Optional[str] = ''


class MemberType(Enum):
    Supporting = 15
    Active = 65



@dataclass
class MemberInfo:
    recipient: Recipient
    quantity: float
    status: MemberType
    extra_prestations: List[Prestation] = field(default_factory=lambda: [])
