from local_types import *


bills = [
    MemberInfo(
        recipient=Recipient(
            id=6969,
            name='Jean Bernard',
            street='Route des Pamplemousse, 14',
            pcode='1220',
            city='Genève',
            country="Suisse",
        ),
        quantity=4,
        status=MemberType.Supporting,
    ),
    MemberInfo(
        recipient=Recipient(
            id=6666,
            name='Mangeur DeSaucisse',
            street='42 Rue de la Wurst',
            pcode='1337',
            city='LeetCity',
            country="France",
        ),
        quantity=8,
        status=MemberType.Active,
        extra_prestations=[
            Prestation(
                name="Ammenage de saucisse sur deux mois",
                quantity=2,
                price=-MemberType.Active.value,
                unit='Mois',
                description="Nomnomnom les bonnes saucisses !"
            ),
        ]
    ),
]