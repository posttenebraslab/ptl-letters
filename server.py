#!/usr/bin/env python
from decimal import Decimal
import io
import base64
from qrbill.bill import QRBill
from stdnum.iso7064 import mod_97_10

from local_types import *
from bills import bills

from flask import Flask, render_template
app = Flask(__name__, static_folder='static')


@app.route("/bill/<id>")
def bill_route(id):
    bill = next(bill for bill in bills if bill.recipient.id == int(id))
    # (recipient, quantity, status) = next((bill['recipient'], bill['quantity'], bill['status']) for bill in bills if bill['recipient'].id == int(id))

    if bill.status == MemberType.Active:
        prestation_name = "Cotisation membre actif 2021"
    else:
        prestation_name = "Cotisation membre de soutien 2021"

    prestations = [
        Prestation(
            name=prestation_name,
            quantity=bill.quantity,
            price=bill.status.value,
            unit='Mois',
        ),
    ]

    if bill.extra_prestations:
        prestations += bill.extra_prestations

    ref_number_version = 0
    ref_number = f'{ref_number_version:1}{bill.recipient.id:04}'
    checksum = 98 - mod_97_10.checksum(ref_number + "RF00")
    ref_number = f'RF{checksum:02}{ref_number}'

    doc_bill = dict(
        reference_number=ref_number,
        prestations=prestations,
        total=Decimal(sum([prestation.quantity * prestation.price for prestation in prestations]))
    )
    qr_bill = QRBill(
        language='fr',
        creditor=dict(
            name='Post Tenebras Lab',
            pcode='1227',
            city='Carouge',
            country='CH',
        ),
        account='CH29 0900 0000 1024 9797 2',
        amount=doc_bill['total'],
        debtor={k:v for k,v in bill.recipient.__dict__.items() if k != 'id'},
        ref_number=ref_number,
    )
    f = io.StringIO()
    qr_bill.as_svg(f)
    qr_bill_svg = base64.b64encode(f.getvalue().encode('utf-8')).decode()
    data=dict(
        recipient=bill.recipient,
        bill=doc_bill,
        qr_bill_svg=qr_bill_svg,
    )
    return render_template('bill.html', **data)


def start_server():
    app.run()

if __name__ == '__main__':
    app.debug = True
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    exit(start_server())
