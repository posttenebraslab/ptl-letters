# ptl-letters

## Setup

Install `pipenv` (see https://github.com/pypa/pipenv#installation for up-to-date instructions), then install the dependencies:

    pipenv install

Copy the `bills.example.py` to `bills.py`, and edit the bills to generate.


## Scripts

Enter a `pipenv` shell to avoid having to prefix every command with `pipenv run`:
```
pipenv shell
```

### `generate_pdfs.py`

Generates PDFs bills in `pdfs/`:
```
./generate_pdfs.py
```

### `server.py`

Run a HTTP server that will generate the letters:
```
./server.py
```

Then you can generate a bill letter in your browser at `http://localhost:5000/bill/id`

Example: http://localhost:5000/bill/1111

## TODO

- use `Decimal` instead of doing floating point operations 
